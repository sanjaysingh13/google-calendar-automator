from datetime import datetime, timedelta
from cal_setup import get_calendar_service


def create_event(date_time,subject,detail):
   # creates one hour event tomorrow 10 AM IST
   service = get_calendar_service()
   start = date_time.isoformat()
   # tomorrow = datetime(d.year, d.month, d.day, 10)+timedelta(days=1)
   # start = tomorrow.isoformat()
   end = (date_time + timedelta(hours=2)).isoformat()

   event_result = service.events().insert(calendarId='primary',
       body={
           "summary": detail,
           "description": subject,
           "start": {"dateTime": start, "timeZone": 'Asia/Kolkata'},
           "end": {"dateTime": end, "timeZone": 'Asia/Kolkata'},
       }
   ).execute()

   print("created event")
   print("id: ", event_result['id'])
   print("summary: ", event_result['summary'])
   print("starts at: ", event_result['start']['dateTime'])
   print("ends at: ", event_result['end']['dateTime'])

# if __name__ == '__main__':
#    main()