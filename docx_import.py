from docx import Document
import datetime
from datetime import timedelta
from dateutil.parser import parse
from create_event import create_event
import re
months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sept","Oct","Nov","Dec"]
weekdays= ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"]
month_matcher = '.*(('+ '|'.join(months) + ').*)' 
module_matcher = '.*Module.{2,6}Weeks.*'
weekday_matcher = '.*(('+ '|'.join(weekdays) + ').*)' 
subject_matcher = 'Module.*Week.*:(.*?)('+'|'.join(months) + ').*'
classes = []
assignments = []
def getText(filename):
	doc = docx.Document(filename)
	fullText = []
	for para in doc.paragraphs:
		fullText.append(para.text)
	return '\n'.join(fullText)
def daterange(date1, date2):
	for n in range(int ((date2 - date1).days)+1):
		yield date1 + timedelta(n)
document = Document('CJUS Data Analysis Course Schedule.docx')

segments = []
segment = []
segments_details = []
for p in document.paragraphs:
	x = re.match(module_matcher, p.text)
	if x:
		if len(segment)!= 0:
			segments.append(segment)
		segment = []
		segment.append(p.text)
	else:
		if len(segment) != 0:
			segment.append(p.text)
if len(segment)!= 0:
	segments.append(segment)
for segment in segments:
	segment_details = {}
	segment_assignment_for = None
	for idx,para in enumerate(segment):
		if idx == 0:
			x = re.findall(r'|'.join(months), para)
			if len(x) == 2:
				date_times = []
				for m in x:
					my_regex =  m + r".{0,4}\d{1,2}"
					md = re.search(my_regex,para)
					date_times.append(parse(md[0]))
			else:
				x = re.match(month_matcher, para)
				dates = re.findall(r"\d+",x[1])
				month = re.match(r"\w+",x[1])[0]
				date_times = [parse(f"{month}{date}") for date in dates]
			segment_details["date_times"] = date_times
			x = re.match(subject_matcher, para)
			subject = x[1].strip()
			segment_details["subject"] = subject
		else:
			if re.match(r".*Class Meetings.*", para):
				y = re.findall(r"|".join(weekdays),para)
				segment_details["weekdays"] = y
				time = re.findall(r"\d+.{0,2}\d+.{0,4}pm|am",para)
				time = parse(time[0].replace(".",":")).time()
				segment_details["time"] = time
			elif re.match(r"Assignments.*", para):
				x = re.match(month_matcher, para)
				assignment_due_date_time = parse(x[1])
				segment_details["assignment_due_date_time"] = assignment_due_date_time
				assignment_for = re.match(r'(Assignments.*)Due.*', para)[1]
				if segment_assignment_for == None:
					segment_assignment_for = assignment_for
	segment_details["assignment_for"] = segment_assignment_for
	segments_details.append(segment_details)
for segment_details in segments_details:
	try:
		time = segment_details["time"]
		for date in daterange(segment_details["date_times"][0],segment_details["date_times"][1]):
			date_weekday = date.weekday()
			if date_weekday in [weekdays.index(weekday) for weekday in segment_details["weekdays"]]:
				classes.append({"date":datetime.datetime.combine(date, time), "subject": segment_details["subject"]})
	except Exception as e:
		print(str(e))
	try:
		assignments.append({"assignment_for":segment_details["assignment_for"], "assignment_due_date_time":datetime.datetime.combine(segment_details["assignment_due_date_time"],datetime.time(12, 0, 0, 0))})
	except:
		pass
print("######################")
for class_room in classes:
	create_event(class_room["date"],"Class",class_room["subject"])
	# print(class_room)
for assignment in assignments:
	create_event(assignment["assignment_due_date_time"],"Assignment",assignment["assignment_for"])
	# print(assignment)
